import React, { Component } from "react";
import ImageUi from "./componenet/imageUi";
import SearchBox from "./componenet/searchBox";
import Loader from "./componenet/loader";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      searchtext: "dog",
    };
  }

  fetchApi = () => {
    fetch(
      `https://api.unsplash.com/search/photos?page=2&query=${
        this.state.searchtext || "car"
      }&client_id=GNJZxCKJyFr3eDd_305YbiL4JyzOFcg6NZYCkSyF-_A`
    )
      .then((res) => res.json())
      .then((data) => this.setState({ data: data }));
  };

  searchbar = (e) => {
    this.setState({
      searchtext: e.target.value,
    });
  };

  componentDidMount() {
    this.fetchApi();
  }
  componentDidUpdate(previousProp, previousState) {
    console.log(previousState);
    if (previousState.searchtext !== this.state.searchtext) {
      this.fetchApi();
    }
  }
  render() {
    return (
      <>
        <SearchBox getText={this.searchbar} />
        {!this.state.data ? <Loader /> : <ImageUi getData={this.state.data} />}
      </>
    );
  }
}
